# SPayMobile

[![CI Status](https://img.shields.io/travis/Julio Molina/SPayMobile.svg?style=flat)](https://travis-ci.org/Julio Molina/SPayMobile)
[![Version](https://img.shields.io/cocoapods/v/SPayMobile.svg?style=flat)](https://cocoapods.org/pods/SPayMobile)
[![License](https://img.shields.io/cocoapods/l/SPayMobile.svg?style=flat)](https://cocoapods.org/pods/SPayMobile)
[![Platform](https://img.shields.io/cocoapods/p/SPayMobile.svg?style=flat)](https://cocoapods.org/pods/SPayMobile)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

SPayMobile is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'SPayMobile'
```

## Author

Julio Molina, julio.molina@outlook.com

## License

SPayMobile is available under the MIT license. See the LICENSE file for more info.
